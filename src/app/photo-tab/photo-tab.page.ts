import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { registerWebPlugin, Plugins } from '@capacitor/core';
import { FileSharer } from '@byteowls/capacitor-filesharer';

@Component({
  selector: 'app-photo-tab',
  templateUrl: 'photo-tab.page.html',
  styleUrls: ['photo-tab.page.scss']
})
export class Tab2Page implements OnInit {

	displayCamera: boolean
	displayAlertMissing: boolean
	displayShareButton: boolean
	userMessage: string

  	constructor(public actionSheetController: ActionSheetController,
			  public screenshot: Screenshot
  )
  {}

  async ngOnInit() {
		this.displayCamera = true
		this.displayAlertMissing = true
		this.displayShareButton = false
		this.userMessage = 'Montre-moi <br />ton dessin !'
		this.initErrorMessageLoader();
		registerWebPlugin(FileSharer);
  }

  public initErrorMessageLoader() {

	window.addEventListener('message', handleMessage, false);

		const self = this

		function handleMessage(event) {
			if (event.data === 'IS_VISIBLE') {
				self.displayAlertMissing = false;
				self.displayShareButton = true;
			} else if (event.data === 'IS_NOT_VISIBLE') {
				self.displayAlertMissing = true;
				self.displayShareButton = false;
				self.userMessage = 'Je ne vois pas <br />ton dessin !'
			}
		}
	}
	
	takeScreenshotURI() {
		this.screenshot.URI(80).then(res => {
			const uri = res.URI;
			this.shareScreenshot(uri);
		});
	}

	shareScreenshot(uri) {
		Plugins.FileSharer.share({
			filename: 'myscreenshot.jpg',
			base64Data: uri,
			contentType: 'image/jpg',
		}).then(() => {
			console.log('hello :D')
		}).catch(error => {
			console.error('File sharing failed', error.message);
		});
	}
}
